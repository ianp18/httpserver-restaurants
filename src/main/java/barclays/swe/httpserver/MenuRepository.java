package barclays.swe.httpserver;

import org.springframework.data.jpa.repository.JpaRepository;

interface MenuRepository extends JpaRepository<Menu, Integer> {

}
