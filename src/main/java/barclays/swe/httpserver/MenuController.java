package barclays.swe.httpserver;

//import java.util.List;

import org.springframework.web.bind.annotation.DeleteMapping;
//import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MenuController {
    private MenuRepository repository;
    private RestaurantRepository restaurantRepository;

    public MenuController(MenuRepository repository, RestaurantRepository restaurantRepository) {
        this.repository = repository;
        this.restaurantRepository = restaurantRepository;
    }

    @PostMapping("/restaurants/{restaurant_id}/menus")
    public Menu addMenu(@RequestBody Menu menuData, @PathVariable Integer restaurant_id) {
        Restaurant restaurant = restaurantRepository.findById(restaurant_id).get();
        menuData.setRestaurant(restaurant);
        return repository.save(menuData);
    }

    @PutMapping("/restaurants/{restaurant_id}/menus/{id}")
    public Menu updateOne(@PathVariable Integer id, @RequestBody Menu menuUpdate) {
        return repository.findById(id).map(menu -> {
            menu.setTitle(menuUpdate.getTitle());
            return repository.save(menu);
        }).orElseThrow();
    }

    @DeleteMapping("/restaurants/{restaurant_id}/menus/{id}")
    public void deleteOne(@PathVariable Integer id) {
        repository.deleteById(id);
    }
}
