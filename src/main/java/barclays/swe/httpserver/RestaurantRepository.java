package barclays.swe.httpserver;

//import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

interface RestaurantRepository extends JpaRepository<Restaurant, Integer> {

    //Optional<Restaurant> findByCountry(String country);
}