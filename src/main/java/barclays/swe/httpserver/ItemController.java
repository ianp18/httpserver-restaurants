package barclays.swe.httpserver;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ItemController {
    private ItemRepository itemRepository;
    private MenuRepository repository;

    public ItemController(ItemRepository itemRepository, MenuRepository repository) {
        this.itemRepository = itemRepository;
        this.repository = repository;
    }

    @PostMapping("/restaurants/{restaurant_id}/menus/{menu_id}/items")
    public Item addItem(@RequestBody Item itemData, @PathVariable Integer menu_id) {
        Menu menu = repository.findById(menu_id).get();
        itemData.setMenu(menu);
        return itemRepository.save(itemData);
    }

    @DeleteMapping("/restaurants/{restaurant_id}/menus/{menu_id}/items/{item_id}")
    public void deleteOne(@PathVariable Integer item_id) {
        itemRepository.deleteById(item_id);
    }
}
