package barclays.swe.httpserver;

import java.util.List;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RestaurantController {
    private RestaurantRepository repository;

    public RestaurantController(RestaurantRepository repository) {
        this.repository = repository;
    }

    @PostMapping("/restaurants")
    public Restaurant createRestaurant(@RequestBody Restaurant newRestaurant) {
        return repository.save(newRestaurant);
    }

    @GetMapping("/restaurants")
    public List<Restaurant> getRestaurants() {
        return this.repository.findAll();
    }

    @GetMapping("/restaurants/{id}")
    public Restaurant getOne(@PathVariable Integer id) {
        return repository.findById(id).get();
    }

    /*@GetMapping("/restuarants/{country}")
    public Restaurant getOne(@PathVariable String country) {
        return repository.findByCountry(country).get();
    }*/

    @PutMapping("/restaurants/{id}")
    public Restaurant updateOne(@PathVariable Integer id, @RequestBody Restaurant restaurantUpdate) {
        return repository.findById(id).map(restaurant -> {
            restaurant.setName(restaurantUpdate.getName());
            restaurant.setImageURL(restaurantUpdate.getImageURL());
            restaurant.setAddress(restaurantUpdate.getAddress());
            restaurant.setCountry(restaurantUpdate.getCountry());
            return repository.save(restaurant);
        }).orElseThrow();
    }

    @DeleteMapping("/restaurants/{id}")
    public void deleteOne(@PathVariable Integer id) {
        repository.deleteById(id);
    }
}
