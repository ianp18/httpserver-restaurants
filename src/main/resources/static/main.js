let allRestaurants = []
let data = []
const state = {
    selectedRestaurant: null
    //newMenu: null
}

fetch('/restaurants')
    .then(response => response.json())
    .then(_data => {
        data = _data
        allRestaurants = _data
    })
    .catch(err => console.error(err))

function getAddFormHtml() {
    return `
        <article>
            <form onsubmit="event.preventDefault();addRestaurant(this);">
                <label>Restaurant Name</label>
                <input name="name" required />
                <label>ImageURL for Restaurant</label>
                <input name="imageURL" type="url" required />
                <label>Restaurant Address</label>
                <input name="address" required />
                <label>Restaurant Country</label>
                <input name="country" required />
                <button style="width: 13rem;">Add Restaurant</button>
             </form>
        </article>
    `
}

function render() {
    let content = data.map((restaurantsData, i) => {
        return `
            <article class= "restaurant-card">
                <div style="background-image: url('${restaurantsData.imageURL}');"></div>
                <h2 id="${i}-header">${restaurantsData.name}</h2>
                <h3 id="${i}-header">${restaurantsData.address}</h3>
                <button onclick="displayMenu(${i})">Menu</button>
            </article >
        `
    }).join("")

    content += getAddFormHtml()
    
    const appEl = document.getElementById('app')
    appEl.innerHTML = content

    if (state.selectedRestaurant) {
        const modalContent = `
            <section class="modal-bg">
                <article>
                    <main>
                        ${state.menuOpen.map(menu => {
                            return `
                                <article>
                                    <h3>${menu.title}</h3>
                                    <ul>
                                        ${menu.items.map(item => {
                                            return `<li>${item.name} £${item.price}</li>`
                                        }).join("")}
                                    </ul>
                                </article>
                            `
                        }).join("")}
                    </main>
                    <footer>
                        <button>Create Menu</button>
                        <button onclick="closeModal()">Close</button>
                    </footer>
                </article>
            </section >
        `
        const modalEl = document.getElementById('modal')
        modalEl.innerHTML = modalContent
    } else {
        const modalEl = document.getElementById('modal')
        modalEl.innerHTML = ""
    }
}

function displayMenu(index) {
    state.selectedRestaurant = data[index]
    render()
}

function getRestaurantByCountry(country) {
    data = allRestaurants.filter(restaurant => {
        return restaurant.country === country
    })
    render()
}

function addRestaurant(HTMLform) {
    const form = new FormData(HTMLform)
    const id = form.get(`id`)
    const name = form.get(`name`)
    const imageURL = form.get(`imageURL`)
    const address = form.get(`address`)
    const country = form.get(`country`)
    fetch('/restaurants', {
        method: 'POST',
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify({name, imageURL, address, country})
    })
    .then(res => res.json())
    .then(restaurant => {
        data.push(restaurant)
        render()
    })
    .catch(console.error)
}



function closeModal() {
    state.menuOpen = null
    render()
}